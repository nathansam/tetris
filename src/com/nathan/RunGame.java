package com.nathan;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class RunGame extends JFrame implements ActionListener {
	
	public static Timer update;
	public static Board board = new Board(22, 10);
	private static JPanel canvas = new JPanel();
	private boolean running = true;
	public static int score = 0, lastScore = 0;
	public static boolean paused = false;
	public static HighScoreTable table;
	public static int speedMils = 900;
	public static Tetromino currentPiece, nextPiece, ghostPiece;
	public static Clip clip;
	public static BufferedImage[] img = new BufferedImage[8];
	public static TetrisMaster tm;

	public static void main(String[] args) {
		RunGame game = new RunGame();
		game.run();
	}
	
	public static JPanel getCanvas() {
		return canvas;
	}
	
	public RunGame() {
		super("Tetris");
	}
	
	public void run() {
		init();
		while (running) {
			draw();
			try{ 
				Thread.sleep(5);
			} catch (InterruptedException e) {};
		}
	}
	
	void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		canvas.setPreferredSize(new Dimension(500, 616));
		InputHandler input = new InputHandler(canvas, this);
		getContentPane().add(canvas);
		
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
		try {
			File soundFile = new File("./res/Tetris.wav");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
		
			clip = AudioSystem.getClip();
		
			clip.open(audioIn);
		} catch (UnsupportedAudioFileException e) {
	       e.printStackTrace();
	    } catch (IOException e) {
	       e.printStackTrace();
	    } catch (LineUnavailableException e) {
	       e.printStackTrace();
	    }
		
		try {
			img[0] = ImageIO.read(new File("./res/Orange.png"));
			img[1] = ImageIO.read(new File("./res/Blue.png"));
			img[2] = ImageIO.read(new File("./res/Purple.png"));
			img[3] = ImageIO.read(new File("./res/Cyan.png"));
			img[4] = ImageIO.read(new File("./res/Yellow.png"));
			img[5] = ImageIO.read(new File("./res/Red.png"));
			img[6] = ImageIO.read(new File("./res/Green.png"));
			img[7] = ImageIO.read(new File("./res/Ghost.png"));
		} catch (IOException e) {};
		
		generatePiece();
		changePiece();
		
		update = new Timer(1000, this);
		update.setInitialDelay(0);
		//update.start();
		
		tm = new TetrisMaster(board, this);
	}
	
	void draw() {
		BufferedImage backBuffer = new BufferedImage(650, 650, BufferedImage.TYPE_INT_RGB);
		
		Graphics g = canvas.getGraphics();
		Graphics bbg = backBuffer.getGraphics();
		
			bbg.setColor(Color.LIGHT_GRAY);
			bbg.fillRect(0, 0, 648, 850);
			board.drawBoard(bbg);
			ghostPiece.draw(bbg);
			currentPiece.draw(bbg);
			bbg.drawString("Next Piece:", 360, 80);
			bbg.drawString("Score: " + score, 360, 400);
			nextPiece.draw(bbg);
			
		if (paused) board.drawPauseScreen(bbg);
		
		g.drawImage(backBuffer, 0, 0, this);
	}
	
	public void restartGame() {
		board = new Board(22, 10);
		speedMils = 900;
		score = 0;
		
		generatePiece();
		changePiece();
		
		update = new Timer(1000, this);
		update.setInitialDelay(0);
		update.start();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == update) {
			if (board.checkGameOver()) {
				update.stop();
				table = new HighScoreTable();
				String name = JOptionPane.showInputDialog(this, "Input your initials:", null);
				if (name != null) {
		        	table.addScore(name, score);
				}
				table.showGraphicsTable();
			}
			else {
				//if (currentPiece.checkCollision(board)) {
					//currentPiece = nextPiece;
					//currentPiece.setCurrentPiece();
					//ghostPiece.setPos(currentPiece);
					//changePiece();
					//tm.setPiece();
					//tm.checkPositions();
				//}
				lastScore = score;
				score += board.checkCompleteRow();
				ghostPiece.dropTile(board);
				updateSpeed();
				//currentPiece.fall();
			}
		}
	}
	
	public void generatePiece() {
		currentPiece = Tetromino.TetrominoFactory.generateTetromino();
		ghostPiece = Tetromino.TetrominoFactory.generateTetromino(currentPiece.type);
		ghostPiece.type = Tetromino.TYPE.GHOST;
		ghostPiece.dropTile(board);
	}
	
	public static void updateTimer() {
		update.stop();
		update.setDelay(speedMils);
		update.setInitialDelay(speedMils);
		update.start();
	}
	
	public void updateSpeed() {
		if (lastScore < 100 && score >= 100) {
			speedMils /= 1.5;
			updateTimer();
		}
		if (lastScore < 200 && score >= 200) {
			speedMils /= 1.5;
			updateTimer();
		}
		if (lastScore < 400 && score >= 400) {
			speedMils /= 1.5;
			updateTimer();
		}
		if (lastScore < 600 && score >= 600) {
			speedMils /= 1.5;
			updateTimer();
		}
		if (lastScore < 800 && score >= 800) {
			speedMils /= 1.25;
			updateTimer();
		}
		if (lastScore < 1000 && score >= 1000) {
			speedMils /= 1.25;
			updateTimer();
		}
		if (lastScore < 1200 && score >= 1200) {
			speedMils /= 1.1;
			updateTimer();
		}
	}
	
	public static void changePiece() {
		nextPiece = Tetromino.TetrominoFactory.generateTetromino();
		nextPiece.setNextPiece();
	}

}
