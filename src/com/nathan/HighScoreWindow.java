package com.nathan;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class HighScoreWindow extends JFrame implements KeyListener{
	
	private String[] table;
	private HighScoreTable t;
	
	public HighScoreWindow(String title, String[] scores, HighScoreTable t) {
		super(title);
		setSize(235, 348);
		setVisible(true);
		setLayout(null);
		
		addKeyListener(this);
		
		this.t = t;
		table = scores;
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, 235, 348);
		g.setColor(Color.red);
		for (int i = 0; i < 20; i++) {
			g.drawRect(7 + i, 30 + i, 220 - 2 * i, 310 - 2 * i);
		}
		g.setFont(new Font("default", Font.BOLD, 18));
		g.drawString("Tetris High Scores:", 32, 70);
		g.setFont(new Font("default", Font.PLAIN, 14));
		g.setColor(Color.black);
		for (int i = 0; i < 10; i++) {
			g.drawString(table[i], 80, i * 20 + 100);
		}
		
	}

	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == 82) {
			System.out.println("here");
			t.reset();
		}
	}

	public void keyReleased(KeyEvent e) {
		
	}

	public void keyTyped(KeyEvent e) {
		
	}

}
