package com.nathan;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class InputHandler {
	
	private boolean keyPressed = false;
	private boolean keysDisabled = false;
	private JPanel canvas;
	private RunGame g;
	
	public InputHandler(JPanel p, RunGame g) {
		this.g = g;
		canvas = p;
		p.getInputMap().put(KeyStroke.getKeyStroke("RIGHT"), "ri");
		p.getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "l");
		p.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "d");
		p.getInputMap().put(KeyStroke.getKeyStroke("UP"), "u");
		p.getInputMap().put(KeyStroke.getKeyStroke("M"), "m");
		p.getInputMap().put(KeyStroke.getKeyStroke("P"), "p");
		p.getInputMap().put(KeyStroke.getKeyStroke("R"), "r");
		p.getInputMap().put(KeyStroke.getKeyStroke("released DOWN"), "releasedD");
		p.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "space");
		p.getActionMap().put("ri", rightArrow);
		p.getActionMap().put("l", leftArrow);
		p.getActionMap().put("d", downArrow);
		p.getActionMap().put("u", upArrow);
		p.getActionMap().put("m", mKey);
		p.getActionMap().put("p", pKey);
		p.getActionMap().put("r", rKey);
		p.getActionMap().put("releasedD", releasedDownArrow);
		p.getActionMap().put("space", space);
	}
	
	Action mKey = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (RunGame.clip.isRunning()) {
				RunGame.clip.stop();
			}
			else {
				RunGame.clip.start();
			}
		}
	};
	
	Action pKey = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!RunGame.update.isRunning() && RunGame.board.checkGameOver()) {
				g.restartGame();
			}
			else if (RunGame.update.isRunning()) {
				RunGame.paused = true;
				RunGame.update.stop();
				keysDisabled = true;
			}
			else if (!RunGame.update.isRunning() && !RunGame.board.checkGameOver()) {
				RunGame.update.start();
				RunGame.paused = false;
				keysDisabled = false;
			}
		}
	};
	
	Action rKey = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (RunGame.table != null && RunGame.table.hasFocus()) {
				RunGame.table.reset();
			}
		}
	};
	
	Action rightArrow = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (!RunGame.board.checkGameOver()) {
					RunGame.currentPiece.moveRight(RunGame.board);
				}
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.ghostPiece.dropTile(RunGame.board);
			}
		}
	};
	
	Action leftArrow = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (!RunGame.board.checkGameOver()) {
					RunGame.currentPiece.moveLeft(RunGame.board);
				}
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.ghostPiece.dropTile(RunGame.board);
			}
		}
	};
	
	Action downArrow = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (!RunGame.board.checkGameOver() && !keyPressed && RunGame.speedMils > 100) {
					RunGame.update.stop();
					RunGame.update.setDelay(100);
					RunGame.update.setInitialDelay(0);
					RunGame.update.start();
					keyPressed = true;
				}
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.ghostPiece.dropTile(RunGame.board);
			}
		}
	};
	
	Action upArrow = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (!RunGame.board.checkGameOver()) {
					RunGame.currentPiece.rotate(RunGame.board);
				}
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.ghostPiece.dropTile(RunGame.board);
			}
		}
	};
	
	Action releasedDownArrow = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (keyPressed && !RunGame.board.checkGameOver()) {
					RunGame.update.stop();
					RunGame.update.setDelay(RunGame.speedMils);
					RunGame.update.setInitialDelay(RunGame.speedMils);
					RunGame.update.start();
					keyPressed = false;
				}
			}
		}
	};
	
	Action space = new AbstractAction() {
		public void actionPerformed(ActionEvent arg0) {
			if (!keysDisabled) {
				if (!RunGame.board.checkGameOver()) {
					RunGame.currentPiece.dropTile(RunGame.board);
					RunGame.update.stop();
					RunGame.update.setInitialDelay(5);
					RunGame.update.start();
				}
			}
		}
	};
	
}
