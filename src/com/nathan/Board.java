package com.nathan;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class Board {

	private LinkedList<Tile[]> grid;
	private int rows, cols;
	private Timer whiteRow = new Timer();
	
	public Board(int rows, int cols) {
		grid = new LinkedList<Tile[]>();
		for (int i = 0; i < rows; i++) {
			grid.add(new Tile[cols]);
		}
		this.rows = rows;
		this.cols = cols;
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				grid.get(i)[j] = new Tile(i, j);
				grid.get(i)[j].setColor(Color.gray);
			}
		}
	}
	
	public int checkCompleteRow() {
		int newPoints = 0;
		int totalPoints = 0;
		int rowCount = 0;
		
		for (int i = 0; i < grid.size(); i++) {
			boolean complete = true;
			for (Tile t : grid.get(i)) {
				if (t.getColor() == Color.gray) {
					complete = false;
					break;
				}
			}
			if (!complete) continue;
			
			removeRow(i);
			newPoints += 10;
			totalPoints += newPoints;
		}
		
		resetTilePos();
		
		return totalPoints;
	}
	
	public void resetTilePos() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				grid.get(i)[j].setPos(i, j);
			}
		}
	}
	
	public void removeRow(int row) {
		grid.remove(row);
		grid.addFirst(new Tile[cols]);
		for (int j = 0; j < cols; j++) {
			grid.get(0)[j] = new Tile(0, j);
			grid.get(0)[j].setColor(Color.gray);
		}
	}
	
	public LinkedList<Tile[]> getBoard() {
		return grid;
	}
	
	public void placePiece(Tetromino piece) {
		Tile[] tiles = piece.getTiles();
		for (Tile t : tiles) {
			grid.get(t.getRow())[t.getCol()].setColor(piece.getColor());
			grid.get(t.getRow())[t.getCol()].setImage(RunGame.img[piece.type.ordinal()]);
		}
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getCols() {
		return cols;
	}
	
	public boolean checkGameOver() {
		for (int j = 0; j < cols; j++) {
			if (grid.get(3)[j].getColor() != Color.gray) return true; 
		}
		return false;
	}
	
	public void drawBoard(Graphics g) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				grid.get(i)[j].drawTile(g);
			}
		}
		//TetrisMaster t = new TetrisMaster(this);
		//t.getPiece().draw(g);
	}
	
	public void drawPauseScreen(Graphics g) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Tile t = new Tile(i, j);
				t.setColor(Color.gray);
				t.drawTile(g);
			}
		}
		g.setColor(Color.RED);
		g.drawString("PAUSED", 120, 300);
	}
	
}
