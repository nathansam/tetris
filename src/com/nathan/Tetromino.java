package com.nathan;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public abstract class Tetromino {
	public Tile[] pieces = new Tile[4];
	public Tile center;
	public TYPE type;
	public enum TYPE {L, J, T, I, O, Z, S, GHOST};
	public Color[] c = {Color.orange, Color.blue, new Color(170, 0, 170), Color.cyan, Color.yellow, Color.red, Color.green, Color.black};
	public Tile[] newPieces;
	
	public boolean checkCollision(Board b) {
		for (Tile t : pieces) {
			if (t.getRow() == b.getRows() - 1 || b.getBoard().get(t.getRow() + 1)[t.getCol()].getColor() != Color.gray) {
				if (this.type != TYPE.GHOST) 
					b.placePiece(this);
				return true;
			}
		}
		return false;
	}
	
	public boolean isValidMove(Board b) {
		boolean canLand = false;
		for (Tile tile : pieces) {
			if (tile.getRow() >= b.getRows() || tile.getCol() >= b.getCols() || 
					tile.getRow() < 0 || tile.getCol() < 0 || 
					b.getBoard().get(tile.getRow())[tile.getCol()].getColor() != Color.gray) {
				return false;
			}
		}
		for (Tile tile : pieces) {
			if (tile.getRow() + 1 == b.getRows()) return true;
			if (tile.getRow() + 1 < b.getRows() && b.getBoard().get(tile.getRow() + 1)[tile.getCol()].getColor() != Color.gray) {
				canLand = true;
			}
		}
		if (!canLand) {
			return false;
		}
		
		return true;
	}
	
	public boolean isInBounds(Board b) {
		for (Tile t : pieces) {
			if (t.getRow() == b.getRows() || t.getRow() == -1 || t.getCol() == b.getCols() || t.getCol() == -1) {
				return false;
			}
		}
		return true;
	}
	
	public void place(TetrisPosition tp) {
		for (int i = 0; i < 4; i++) {
			pieces[i].setPos(tp.getPositions()[i][0], tp.getPositions()[i][1]);
		}
	}

	public void rotate(Board b) {
		if (type == TYPE.O) return;
		
		newPieces = new Tile[4];
		
		for (int i = 0; i < 4; i++) {
			Tile t = pieces[i];
			if (t != null) {
				newPieces[i] = new Tile(t);
			}
		}
		
		for (Tile t : newPieces) {
			t.setPos(center.getRow() + (t.getCol() - center.getCol()), center.getCol() - (t.getRow() - center.getRow()));
		}
		
		if (!checkOverlap(b)) 
			fixOverlap(b);
		else {
			placePieces();
		}
	}
	
	public void rotate() {
		if (type != TYPE.O) {
			for (Tile t : pieces) {
				t.setPos(center.getRow() + (t.getCol() - center.getCol()), center.getCol() - (t.getRow() - center.getRow()));
			}
			boolean outOfBounds = true;
			while (outOfBounds) {
				boolean[] inBounds = {false, false, false, false};
				for (int i = 0; i < pieces.length; i++) {
					if (pieces[i].getRow() < 0) {
						fall();
					}
					else if (pieces[i].getCol() < 0) {
						moveRight();
					}
					else if (pieces[i].getCol() > RunGame.board.getCols() - 1) {
						moveLeft();
					}
					else {
						inBounds[i] = true;
					}
				}
				int counter = 0;
				for (int i = 0; i < inBounds.length; i++) {
					if (inBounds[i]) {
						counter++;
					}
				}
				if (counter == 4) outOfBounds = false;
			}
		}
	}
	
	public void dropTile(Board b) {
		while(!checkCollision(b)) {
			fall();
		}
	}
	
	public Tile[] getTiles() {
		return pieces;
	}
	
	public Color getColor() {
		return c[type.ordinal()];
	}
	
	public void draw(Graphics g) {
		for (Tile t : pieces) {
			t.setColor(c[type.ordinal()]);
			t.setImage(RunGame.img[type.ordinal()]);
			t.drawTile(g);
		}
	}
	
	public void setPos(Tetromino te) {
		for (int i = 0; i < 4; i++) {
			pieces[i].setPos(te.pieces[i].getRow(), te.pieces[i].getCol());
		}
	}
	
	public void setCurrentPiece() {
		for (Tile t : pieces) {
			t.setPos(t.getRow() - 5, t.getCol() - 8);
		}
	}
	
	public void setNextPiece() {
		for (Tile t : pieces) {
			t.setPos(t.getRow() + 5, t.getCol() + 8);
		}
	}
	
	public void moveRight(Board b) {
		for (Tile t : pieces) {
			if (t.getCol() == 9 || b.getBoard().get(t.getRow())[t.getCol() + 1].getColor() != Color.gray) return;
		}
		for (Tile t : pieces) {
			t.moveRight();
		}
	}
	
	public void moveLeft(Board b) {
		for (Tile t : pieces) {
			if (t.getCol() == 0 || b.getBoard().get(t.getRow())[t.getCol() - 1].getColor() != Color.gray) return;
		}
		for (Tile t : pieces) {
			t.moveLeft();
		}
	}
	
	public void moveLeft() {
		for (Tile t : pieces) {
			t.moveLeft();
		}
	}
	
	public void moveRight() {
		for (Tile t : pieces) {
			t.moveRight();
		}
	}
	
	public void moveUp() {
		for (Tile t : pieces) {
			t.decrementCol();
		}
	}
	
	public void fall() {
		for (Tile t : pieces) {
			t.incrementCol();
		}
	}
	
	public boolean checkOverlap(Board b) {
		for (Tile tile : newPieces) {
			if (tile.getRow() >= b.getRows() || tile.getCol() >= b.getCols() || 
					tile.getRow() < 0 || tile.getCol() < 0 || 
					b.getBoard().get(tile.getRow())[tile.getCol()].getColor() != Color.gray) {
				return false;
			}
		}
		
		return true;
	}
	
	public void fixOverlap(Board b) {
		int[][] pos = new int[4][2];
		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				if (j == 0) 
					pos[i][j] = newPieces[i].getRow();
				else
					pos[i][j] = newPieces[i].getCol();
			}
		}
		
		int rowCount = 0;
		int i = 0;
		
		if (this.type != TYPE.I) {
			while (i != -2) {
				for (int j = -1; j < 2; j++) {
					for (Tile t : newPieces) {
						t.setPos(pos[rowCount][0] + i, pos[rowCount][1] + j);
						rowCount++;
					}
					if (checkOverlap(b)) {
						placePieces();
						return;
					}
					rowCount = 0;
				}
				switch (i) {
				case 0: i = -1; break;
				case -1: i = 1; break;
				case 1: i = -2; break;
				}
			}
		}
		else {
			while (i != 3) {
				for (int j = -1; j < 2; j++) {
					for (Tile t : newPieces) {
						t.setPos(pos[rowCount][0] + j, pos[rowCount][1] + i);
						rowCount++;
					}
					if (checkOverlap(b)) {
						placePieces();
						return;
					}
					rowCount = 0;
				}
				switch (i) {
				case 0: i = -1; break;
				case -1: i = 1; break;
				case 1: i = 2; break;
				case 2: i = -2; break;
				case -2: i = 3; break;
				}
			}
		}
	}
	
	public void placePieces() {
		for (int i = 0; i < 4; i++) {
			pieces[i].setPos(newPieces[i].getRow(), newPieces[i].getCol());
		}
	}
	
	public static class TetrominoFactory {
		
		public static Tetromino generateTetromino(TYPE t) {
			switch (t) {
				case L: return new LPiece();
				case J: return new JPiece();
				case T: return new TPiece();
				case I: return new IPiece();
				case O: return new OPiece();
				case Z: return new ZPiece();
				case S: return new SPiece();
			}
			return new LPiece();
		}
		
		public static Tetromino generateTetromino() {
			int n = (int)(Math.random() * 7);
			switch (n) {
				case 0: return new LPiece();
				case 1: return new JPiece();
				case 2: return new TPiece();
				case 3: return new IPiece();
				case 4: return new OPiece();
				case 5: return new ZPiece();
				case 6: return new SPiece();
			}
			return new LPiece();
		}
		
	}
}
