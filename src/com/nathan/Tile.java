package com.nathan;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Tile {
	
	private int row, col;
	private Color color;
	private BufferedImage currentImage;
	
	public Tile(int row, int col) {
		this.col = col;
		this.row = row;
	}
	
	public Tile (Tile t) {
		row = t.getRow();
		col = t.getCol();
	}
	
	public void setPos(int r, int c) {
		row = r;
		col = c;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}
	
	public void incrementCol() {
		row++;
	}
	
	public void decrementCol() {
		row--;
	}
	
	public void moveRight() {
		col++;
	}
	
	public void moveLeft() {
		col--;
	}
	
	public BufferedImage getImage() {
		return currentImage;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color c) {
		color = c;
	}
	
	public void setImage(BufferedImage img) {
		currentImage = img;
	}
	
	public void drawTile(Graphics g) {
		g.setColor(color);
		g.fillRect(col * 30, row * 30 - 45, 30, 30);
		g.setColor(Color.black);
		g.drawRect(col * 30, row * 30 - 45, 30, 30);
		g.drawImage(currentImage, col * 30, row * 30 - 45, null);
	}
	
}
