package com.nathan;

import java.awt.Color;

public class TPiece extends Tetromino{
	
	private int rotation = 0;

	public TPiece() {
		this.type = TYPE.T;
		
		pieces[0] = new Tile(1, 5);
		pieces[1] = new Tile(0, 4);
		pieces[2] = new Tile(0, 5);
		pieces[3] = new Tile(0, 6);
		
		center = pieces[2];
	}
}
