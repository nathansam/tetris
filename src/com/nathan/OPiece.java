package com.nathan;

public class OPiece extends Tetromino{

	public OPiece() {
		this.type = TYPE.O;
		
		pieces[0] = new Tile(1, 4);
		pieces[1] = new Tile(0, 4);
		pieces[2] = new Tile(0, 5);
		pieces[3] = new Tile(1, 5);
	}

	@Override
	public void rotate(Board b) {
		// TODO Auto-generated method stub
		
	}
	
}
