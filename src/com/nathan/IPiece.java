package com.nathan;

import java.awt.Color;

public class IPiece extends Tetromino{

	private int rotation = 0;
	
	public IPiece() {
		this.type = TYPE.I;
		
		pieces[0] = new Tile(0, 3);
		pieces[1] = new Tile(0, 4);
		pieces[2] = new Tile(0, 5);
		pieces[3] = new Tile(0, 6);
		
		center = pieces[2];
	}
}
