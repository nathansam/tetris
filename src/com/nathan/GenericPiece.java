package com.nathan;

public class GenericPiece extends Tetromino {

	public GenericPiece(TetrisPosition tp, TYPE t) {
		for (int i = 0; i < tp.getPositions().length; i++) {
			pieces[i] = new Tile(tp.getPositions()[i][0], tp.getPositions()[i][1]);
		}
		if (t != TYPE.Z)
			center = pieces[2];
		else 
			center = pieces[1];
	}
	
}
