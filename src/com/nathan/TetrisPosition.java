package com.nathan;

import java.awt.Color;

public class TetrisPosition {
	
	private int[][] positions = new int[4][2];

	public TetrisPosition(Tetromino t) {
		for (int i = 0; i < 4; i++) {
			positions[i][0] = t.getTiles()[i].getRow();
			positions[i][1] = t.getTiles()[i].getCol();
		}
	}
	
	public TetrisPosition(Integer[][] pos) {
		for (int i = 0; i < 4; i++) {
			positions[i][0] = pos[i][0];
			positions[i][1] = pos[i][1];
		}
	}
	
	public boolean isValidMove(Board b) {
		for (int i = 0; i < positions.length; i++) {
				if (b.getBoard().get(positions[i][0])[positions[i][1]].getColor() != Color.gray) return false;
		}
		return true;
	}
	
	public boolean canMoveTo(TetrisPosition tp, Tetromino.TYPE t) {
		boolean left = true;
		boolean right = true;
		boolean down = true;
		boolean rotated = false;
		for (int i = 0; i < tp.getPositions().length; i++) {
			if (positions[i][1] - tp.getPositions()[i][1] != 1 || positions[i][0] != tp.getPositions()[i][0]) {
				left = false;
			}
		}
		for (int i = 0; i < tp.getPositions().length; i++) {
			if (positions[i][1] - tp.getPositions()[i][1] != -1 || positions[i][0] != tp.getPositions()[i][0]) {
				right = false;
			}
		}
		for (int i = 0; i < tp.getPositions().length; i++) {
			if (positions[i][0] - tp.getPositions()[i][0] != -1 || positions[i][1] != tp.getPositions()[i][1]) {
				down = false;
			}
		}
		GenericPiece gp = new GenericPiece(tp, t);
		for (int i = 0; i < 3; i++) {
			gp.rotate();
			int counter = 0;
			for (int j = 0; j < 4; j++) {
				if (gp.getTiles()[j].getRow() == positions[j][0] && gp.getTiles()[j].getCol() == positions[j][1]) {
					counter++;
				}
				if (counter == 4) {
					rotated = true;
					break;
				}
			}
			if (rotated == true) break;
		}
		return left || right || down || rotated;
	}
	
	public String toString() {
		String result = "";
		for (int i = 0; i < 4; i++) {
			result += "{" + positions[i][0] + "," + positions[i][1] + "} ";
		}
		return result;
	}
	
	public boolean equals(TetrisPosition other) {
		for (int i = 0; i < positions.length; i++) {
			for (int j = 0; j < positions[0].length; j++) {
				if (positions[i][j] != other.getPositions()[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	public int[][] getPositions() {
		return positions;
	}
	
}
