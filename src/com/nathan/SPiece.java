package com.nathan;

import java.awt.Color;

public class SPiece extends Tetromino{
	
	private int rotation = 0;

	public SPiece() {
		this.type = TYPE.S;
		
		pieces[0] = new Tile(1, 4);
		pieces[1] = new Tile(1, 5);
		pieces[2] = new Tile(0, 5);
		pieces[3] = new Tile(0, 6);
		
		center = pieces[1];
	}
}
