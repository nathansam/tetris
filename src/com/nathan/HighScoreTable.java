package com.nathan;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class HighScoreTable extends JFrame {
   private String path = "./res/tableTetris.txt";
   private File file = new File(path);
   private Scanner input = null;
   private PrintWriter output = null;
   private int tableLength = 10;
   private String[] table = new String[11];
   
   public HighScoreTable() {
	  System.out.println(file.getPath());
      try {
         input = new Scanner(file);
      }
      catch(FileNotFoundException ex){
         createTable();
      }
   }
   
   public void showTable() {
      try {
         input = new Scanner(file);
      }
      catch(FileNotFoundException ex){
         createTable();
      }
      while (input.hasNextLine()){
         System.out.println(input.nextLine());
      }
      input.close();
   }
   
   public void showGraphicsTable() {
	   try {
		   input = new Scanner(file);
	   }
	   catch (FileNotFoundException ex) {
		   createTable();
	   }
	   String[] scores = new String[10];
	   int c = 0;
	   while (input.hasNextLine()) {
		   scores[c] = input.nextLine();
		   c++;
	   }
	   HighScoreWindow j = new HighScoreWindow("Tetris High Scores", scores, this);
   }
   
   public void createTable() {
      System.out.println("Making new table...");
      try {
         output = new PrintWriter(file);
      }
      catch(FileNotFoundException ex){
         System.out.println("Cannot create file " + path);
         return;
      }
      reset();
   }  
   
   public void addScore(int score) {
      Scanner s = new Scanner(System.in);
      String initial;
      do {
         System.out.println("Input your initials");
         initial = s.nextLine();
         if (initial.length() != 3)
         {
            System.out.println("Your initials should be three letters long");
         }
      }while(initial.length() != 3);
      try {
         input = new Scanner(file);
      }
      catch(FileNotFoundException ex) {
         createTable();
         return;
      }
      int c = 0;
      while (input.hasNextLine() && c < 10){
         table[c] = input.nextLine();
         c++;
      }
      input.close();
      try {
         output = new PrintWriter(file);
      }
      catch(FileNotFoundException ex) {
         System.out.println("Cannot open file " + path);
         return;
      }
      table[10] = initial.toUpperCase() + " " + score;
      for (int i = 0; i < table.length-1; i++) {
         for (int j = i + 1; j < table.length; j++) {
            if (Integer.parseInt(table[i].substring(4, table[i].length())) < Integer.parseInt(table[j].substring(4, table[j].length()))){
               String temp = table[i];
               table[i] = table[j];
               table[j] = temp;
            }
         }
      }
      for (int i = 0; i < 10; i++) output.println(table[i]);
      output.close();
   }
   
   public void addScore(String initials, int score) {
	      do {
	         if (initials.length() != 3)
	         {
	            initials = JOptionPane.showInputDialog(this, "Your initials should be three letters. Try again:");
	         }
	      }while(initials.length() != 3);
	      try {
	         input = new Scanner(file);
	      }
	      catch(FileNotFoundException ex) {
	         createTable();
	         return;
	      }
	      int c = 0;
	      while (input.hasNextLine() && c < 10){
	         table[c] = input.nextLine();
	         c++;
	      }
	      input.close();
	      try {
	         output = new PrintWriter(file);
	      }
	      catch(FileNotFoundException ex) {
	         System.out.println("Cannot open file " + path);
	         return;
	      }
	      table[10] = initials.toUpperCase() + " " + score;
	      for (int i = 0; i < table.length-1; i++) {
	         for (int j = i + 1; j < table.length; j++) {
	            if (Integer.parseInt(table[i].substring(4, table[i].length())) < Integer.parseInt(table[j].substring(4, table[j].length()))){
	               String temp = table[i];
	               table[i] = table[j];
	               table[j] = temp;
	            }
	         }
	      }
	      for (int i = 0; i < 10; i++) output.println(table[i]);
	      output.close();
	   }
   
   public void askToReset()
   {
      System.out.println("Would you like to reset the high score table?");
      Scanner s = new Scanner(System.in);
      String choice = s.nextLine();
      if (choice.substring(0, 1).toUpperCase().equals("Y")) {
         reset();
      }
   }
   
   public void reset()
   {
	  String password = JOptionPane.showInputDialog(this, "Input the password", null);
	  if (password.equals("nathanisawesome")) {
		  try {
			  output = new PrintWriter(file);
		  }
		  catch (FileNotFoundException ex) {
			  System.out.println("Could not find file " + path);
			  return;
		  }
		  output.print("AAA ");
		  output.println(0);
		  output.print("BBB ");
		  output.println(0);
		  output.print("CCC ");
		  output.println(0);
		  output.print("DDD ");
		  output.println(0);
		  output.print("EEE ");
		  output.println(0);
		  output.print("FFF ");
		  output.println(0);
		  output.print("GGG ");
		  output.println(0);	
		  output.print("HHH ");
		  output.println(0);
		  output.print("III ");
		  output.println(0);
		  output.print("JJJ ");
		  output.println(0);
		  output.close();
	  }
   }   
}