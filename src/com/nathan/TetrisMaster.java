package com.nathan;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.Timer;

public class TetrisMaster implements ActionListener{

	private Tetromino activePiece;
	private LinkedList<TetrisPosition> path = new LinkedList<TetrisPosition>();
	private int pathCounter = 0;
	private HashMap<Integer, Integer[][]> positions = new HashMap<Integer, Integer[][]>();
	private HashMap<Integer, Integer[][]> validPositions = new HashMap<Integer, Integer[][]>();
	private HashMap<Integer, Integer[][]> checkedPos = new HashMap<Integer, Integer[][]>();
	private HashMap<Integer, Integer> rotations = new HashMap<Integer, Integer>();
	private int[] ratings;
	private int posToUse;
	private int curRotation = 1;
	private int checkedCounter = 0;
	private int counter = 0;
	private int validCounter = 0;
	private Timer rotate;
	private Timer move;
	
	private RunGame game;
	private Board board;
	
	public TetrisMaster(Board b, RunGame rg) {
		setPiece();
		board = b;
		game = rg;
		rotate = new Timer(300, this);
		move = new Timer(300, this);
		checkPositions();
	}
	
	public void setPiece() {
		RunGame.updateTimer();
		activePiece = Tetromino.TetrominoFactory.generateTetromino(RunGame.currentPiece.type);
		checkedPos.clear();
		checkedCounter = 0;
		path.clear();
		pathCounter = 0;
	}
	
	public void setChecked(TetrisPosition tp) {
		Integer[][] pos = new Integer[4][2];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				pos[i][j] = tp.getPositions()[i][j];
			}
		}
		checkedPos.put(checkedCounter, pos);
		checkedCounter++;
	}
	
	public boolean isChecked(TetrisPosition tp) {
		boolean isChecked = false;
		for (int i = 0; i < checkedPos.size(); i++) {
			if (tp.equals(new TetrisPosition(checkedPos.get(i)))) {
				isChecked = true;
			}
		}
		return isChecked;
	}
	
	public boolean isGoalPosReachable(TetrisPosition current, TetrisPosition goal) {
		path.add(current);
		pathCounter++;
		if (current.equals(goal)) return true;
		if (!current.isValidMove(board)) {
			pathCounter--;
			path.remove(pathCounter);
			return false;
		}
		setChecked(current);
		
		for (TetrisPosition nextPos : getPossiblePositions(current)) {
			if (!isChecked(nextPos)) {
				if (isGoalPosReachable(nextPos, goal)) {
					return true;
				}
				else {
					
				}
			}
		}
		pathCounter--;
		path.remove(pathCounter);
		return false;
	}
	
	public LinkedList<TetrisPosition> getPossiblePositions(TetrisPosition tp) {
		LinkedList<TetrisPosition> pos = new LinkedList<TetrisPosition>();
		for (int i = 0; i < positions.size(); i++) {
			if (tp.canMoveTo(new TetrisPosition(positions.get(i)), activePiece.type)) {
				pos.add(new TetrisPosition(positions.get(i)));
			}
		}
		return pos;
	}
	
	public Tetromino getPiece() {
		return activePiece;
	}
	
	public void checkPositions() {
		moveToCorner();
		counter = 0;
		validCounter = 0;
		validPositions.clear();
		positions.clear();
		rotations.clear();
		int direction = 1;
		int rotation = 1;
		while (rotation <= 4) {
			while(activePiece.isInBounds(board)) {
				//if (activePiece.isValidMove(board)) {
					positions.put(counter, new Integer[4][2]);
					rotations.put(counter, rotation);
					for (int j = 0; j < 4; j++) {
						positions.get(counter)[j][0] = activePiece.pieces[j].getRow();
						positions.get(counter)[j][1] = activePiece.pieces[j].getCol();
					}
					if (activePiece.isValidMove(board)) {
						validPositions.put(validCounter, new Integer[4][2]);
						for (int j = 0; j < 4; j++) {
							validPositions.get(validCounter)[j][0] = activePiece.pieces[j].getRow();
							validPositions.get(validCounter)[j][1] = activePiece.pieces[j].getCol();
						}
						validCounter++;
					}
					counter++;
				//}
				if (direction == 1) activePiece.moveRight();
				if (direction == 2) activePiece.moveLeft();
				if (!activePiece.isInBounds(board)) {
					if (direction == 1) {
						activePiece.moveLeft();
						activePiece.moveUp();
						direction = 2;
					}
					else if (direction == 2) {
						activePiece.moveRight();
						activePiece.moveUp();
						direction = 1;
					}
				}
			/*
			activePiece.draw(RunGame.getCanvas().getGraphics());
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			}
			if (direction == 1) activePiece.moveLeft();
			if (direction == 2) activePiece.moveRight();
			activePiece.rotate();
			moveToCorner();
			direction = 1;
			rotation++;
		}
		ratePositions();
		//posToUse = (int)(Math.random() * positions.size());
		/*
		for (int i = 0; i < rotations.get(posToUse); i++) {
			RunGame.currentPiece.rotate(board);
		}
		*/
		System.out.println(new TetrisPosition(validPositions.get(posToUse)));
		//System.out.println(isGoalPosReachable(new TetrisPosition(RunGame.currentPiece), new TetrisPosition(validPositions.get(posToUse))));
		boolean canReach = isGoalPosReachable(new TetrisPosition(RunGame.currentPiece), new TetrisPosition(validPositions.get(posToUse)));
		System.out.println(canReach);
		while (!canReach) {
			ratings[posToUse] = -1000;
			checkedPos.clear();
			checkedCounter = 0;
			pickPosition();
			canReach = isGoalPosReachable(new TetrisPosition(RunGame.currentPiece), new TetrisPosition(validPositions.get(posToUse)));
		}
		//if (!canReach) {
		//	RunGame.update.stop();
		//}
		for (int i = 0; i < path.size(); i++) {
			RunGame.currentPiece.place(path.get(i));
			RunGame.currentPiece.draw(RunGame.getCanvas().getGraphics());
			RunGame.ghostPiece.setPos(RunGame.currentPiece);
			RunGame.ghostPiece.dropTile(board);
			if (i == path.size() - 1) {
				RunGame.board.placePiece(RunGame.currentPiece);
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.currentPiece = RunGame.nextPiece;
				RunGame.currentPiece.setCurrentPiece();
				RunGame.ghostPiece.setPos(RunGame.currentPiece);
				RunGame.changePiece();
				setPiece();
				RunGame.score += board.checkCompleteRow();
				checkPositions();
			}
			game.draw();
			
			try {
				Thread.sleep(2);
			}
			catch (InterruptedException e) {};
			
		}
		//rotate.restart();
		//System.out.println(rotations.get(posToUse));
		/*
		for (int i = 0; i < Math.abs(positions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol()); i++) {
			if (positions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol() < 0)
				RunGame.currentPiece.moveLeft();
			if (positions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol() > 0)
				RunGame.currentPiece.moveRight();
		}
		*/
		/*
		if (!RunGame.board.checkGameOver() && RunGame.speedMils > 100) {
			RunGame.update.stop();
			RunGame.update.setDelay(100);
			RunGame.update.setInitialDelay(0);
			RunGame.update.start();
		}
		*/
	}
	
	public boolean partOfPiece(int r, int c, int pos) {
		for (int i = 0; i < 4; i++) {
			if (validPositions.get(pos)[i][0] == r && validPositions.get(pos)[i][1] == c) return true;
		}
		return false;
	}
	
	public boolean checkFullRow(int r, int pos) {
		for(int i = 0; i < board.getCols(); i++) {
			if (board.getBoard().get(r)[i].getColor() == Color.gray) {
				if (!partOfPiece(r, i, pos)) {
					return false;
				}
			}
		}
		return true;
	}
	
	public void ratePositions() {
		ratings = new int[validPositions.size()];
		LinkedList<Integer> checkedRows = new LinkedList<Integer>();
		for (int i = 0; i < ratings.length; i++) {
			ratings[i] = 0;
		}
		for (int i = 0; i < validPositions.size(); i++) {
			boolean fits = true;
			for (int j = 0; j < 4; j++) {
				int row = validPositions.get(i)[j][0];
				int col = validPositions.get(i)[j][1];
				if (row + 1 < board.getRows() && !partOfPiece(row + 1, col, i) && board.getBoard().get(row + 1)[col].getColor() == Color.gray) {
					ratings[i] -= 2;
					for (int k = row + 2; k < board.getRows(); k++) {
						if (board.getBoard().get(k)[col].getColor() == Color.gray) {
							ratings[i] -= 2;
						}
					}
				}
				if (!checkedRows.contains(row)) {
					if (checkFullRow(row, i)) {
						ratings[i] += 4;
						checkedRows.add(row);
					}
				}
				if (row < 10) ratings[i]--;
				if (row - 1 >= 0) {
					if (board.getBoard().get(row - 1)[col].getColor() == Color.gray) fits = false;
				}
				if (row + 1 < board.getRows()) {
					if (board.getBoard().get(row + 1)[col].getColor() == Color.gray) fits = false;
				}
			}
			if (fits) ratings[i] += 4;
			checkedRows.clear();
		}
		pickPosition();
	}
	
	public void pickPosition() {
		int pos = 0;
		int max = ratings[0];
		for (int i = 0; i < ratings.length; i++) {
			//System.out.println(ratings[i]);
			if (ratings[i] > max) {
				pos = i;
				max = ratings[i];
			}
		}
		posToUse = pos;
		//System.out.println(ratings[pos]);
		//System.out.println(ratings[posToUse]);
	}
	
	//The bottom left corner will be a starting point for checking available positions
	public void moveToCorner() {
		while (activePiece.isInBounds(board)) {
			activePiece.fall();
		}
		activePiece.moveUp();
		while (activePiece.isInBounds(board)) {
			activePiece.moveLeft();
		}
		activePiece.moveRight();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == rotate) {
			if (curRotation < rotations.get(posToUse)) {
				RunGame.currentPiece.rotate();
				curRotation++;
			}
			else {
				curRotation = 1;
				rotate.stop();
				move.restart();
			}
		}
		if (arg0.getSource() == move) {
			if (validPositions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol() < 0) {
				RunGame.currentPiece.moveLeft(board);
			}
			else if (validPositions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol() > 0) {
				RunGame.currentPiece.moveRight(board);
			}
			else if (validPositions.get(posToUse)[0][1] - RunGame.currentPiece.pieces[0].getCol() == 0) {
				move.stop();
				if (!RunGame.board.checkGameOver() && RunGame.speedMils > 100) {
					RunGame.update.stop();
					RunGame.update.setDelay(100);
					RunGame.update.setInitialDelay(0);
					RunGame.update.start();
				}
			}
		}
		RunGame.ghostPiece.setPos(RunGame.currentPiece);
		RunGame.ghostPiece.dropTile(RunGame.board);
	}
	
}
