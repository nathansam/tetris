package com.nathan;

import java.awt.Color;

public class ZPiece extends Tetromino{
	
	private int rotation = 0;
	
	public ZPiece() {
		this.type = TYPE.Z;
		
		pieces[0] = new Tile(0, 4);
		pieces[1] = new Tile(0, 5);
		pieces[2] = new Tile(1, 5);
		pieces[3] = new Tile(1, 6);
		
		center = pieces[1];
	}
}
