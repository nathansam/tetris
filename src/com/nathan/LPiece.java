package com.nathan;

import java.awt.Color;

public class LPiece extends Tetromino{
	
	private int rotation = 0;

	public LPiece() {
		this.type = TYPE.L;
		
		pieces[0] = new Tile(1, 4);
		pieces[1] = new Tile(0, 4);
		pieces[2] = new Tile(0, 5);
		pieces[3] = new Tile(0, 6);
		
		center = pieces[2];
	}
}
